import React from "react";
import { AppProps } from "next/app";
import dataProvider from "src/dataProvider";
import { Refine } from "@pankod/refine-core";
import {
  notificationProvider,
  ReadyPage,
  ErrorComponent,
  LoginPage,
} from "@pankod/refine-antd";
import routerProvider from "@pankod/refine-nextjs-router";

require("antd/dist/antd.less");

// import dataProvider from "@pankod/refine-graphql";
import { GraphQLClient } from "graphql-request";
const API_URL = "http://localhost:3001/api/graphql";
const client = new GraphQLClient(API_URL);
const gqlDataProvider = dataProvider(client);
import { authProvider } from "src/authProvider";
import {
  Title,
  Header,
  Sider,
  Footer,
  Layout,
  OffLayoutArea,
} from "@components/layout";
import PostList from "@components/posts/PostList";
import PostCreate from "@components/posts/PostCreate";
import PostEdit from "@components/posts/PostEdit";
import PostShow from "@components/posts/PostShow";
import TagList from "@components/tags/TagList";
import TagCreate from "@components/tags/TagCreate";
import TagEdit from "@components/tags/TagEdit";
import TagShow from "@components/tags/TagShow";

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <Refine
      routerProvider={routerProvider}
      notificationProvider={notificationProvider}
      ReadyPage={ReadyPage}
      catchAll={<ErrorComponent />}
      dataProvider={gqlDataProvider}
      authProvider={authProvider}
      LoginPage={LoginPage}
      Title={Title}
      Header={Header}
      Sider={Sider}
      Footer={Footer}
      Layout={Layout}
      OffLayoutArea={OffLayoutArea}
      resources={[
        {
          name: 'posts',
          list: PostList,
          create: PostCreate,
          edit: PostEdit,
          show: PostShow,
          canDelete: true
        },
        {
          name: 'users'
        },
        {
          name: 'tags',
          list: TagList,
          create: TagCreate,
          edit: TagEdit,
          show: TagShow,
          canDelete: true
        }
      ]}
    >
      <Component {...pageProps} />
    </Refine>
  );
}

export default MyApp;
