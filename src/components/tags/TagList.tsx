import { Button, Col, EditButton, Form, Input, List, Row, ShowButton, Space, Table, useTable } from '@pankod/refine-antd'
import { CrudFilters } from '@pankod/refine-core'
import Link from 'next/link'
import React from 'react'
import { IPost } from 'src/interfaces/post'

type Props = {}

const TagList = (props: Props) => {
  const {
    tableProps,
    searchFormProps
  } = useTable({
    metaData: {
      fields: ['id', 'name']
    },
    onSearch: (data: any) => {
      const filters: CrudFilters = [];
      if (data.name) {
        filters.push({
          field: "name",
          operator: "contains",
          value: data.name,
        })
      }
      return filters
    }
  })
  return (
    <List>
      <Row gutter={[16,16]}>
        <Col xs={24}>
          <Form {...searchFormProps} layout="vertical">
            <Form.Item name="name" label="Name">
              <Input />
            </Form.Item>
            <Form.Item>
              <Button htmlType="submit" type="primary">Filter</Button>
            </Form.Item>
          </Form>
        </Col>
        <Col xs={24}>
          <Table {...tableProps}>
            <Table.Column dataIndex="id" title="ID" sorter />
            <Table.Column dataIndex="name" title="Name" sorter />
            <Table.Column
              title={'Actions'}
              // dataIndex="actions"
              render={(record): React.ReactNode => {
                return (
                  <Space>
                    <EditButton
                      size="small"
                      recordItemId={record.id}
                    />
                    <ShowButton
                      size="small"
                      recordItemId={record.id}
                    />
                  </Space>
                );
              }}
            />
          </Table>
        </Col>
      </Row>
    </List>
  )
}

export default TagList