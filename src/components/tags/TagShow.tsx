import { Show, Typography } from '@pankod/refine-antd'
import { useShow } from '@pankod/refine-core'
import React from 'react'

type Props = {}

const TagShow = (props: Props) => {
  const {
    queryResult
  } = useShow({
    metaData: {
      fields: ['id', 'name']
    }
  })
  const { data, isLoading } = queryResult;
  const record = data?.data;
  return (
    <Show isLoading={isLoading}>
      <Typography.Title level={5}>ID</Typography.Title>
      <Typography.Text>{record?.id}</Typography.Text>

      <Typography.Title level={5}>Name</Typography.Title>
      <Typography.Text>{record?.name}</Typography.Text>
    </Show>
  )
}

export default TagShow