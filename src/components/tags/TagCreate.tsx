import { Create, Form, Input, useForm } from '@pankod/refine-antd'
import React from 'react'

type Props = {}

const TagCreate = (props: Props) => {
  const {
    saveButtonProps,
    formProps
  } = useForm({
    redirect: 'list'
  })
  return (
    <Create saveButtonProps={saveButtonProps}>
      <Form {...formProps}>
        <Form.Item name="name" label="Name">
          <Input />
        </Form.Item>
      </Form>
    </Create>
  )
}

export default TagCreate