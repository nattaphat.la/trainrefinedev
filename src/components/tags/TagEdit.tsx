import { Edit, Form, Input, useForm } from '@pankod/refine-antd'
import React from 'react'

const TagEdit = (props: any) => {
  const {
    saveButtonProps,
    formProps
  } = useForm({
    metaData: {
      fields: ['id', 'name']
    }
  })
  return (
    <Edit {...props} saveButtonProps={saveButtonProps}>
      <Form {...formProps}>
        <Form.Item name="name" label="Name">
          <Input />
        </Form.Item>
      </Form>
    </Edit>
  )
}

export default TagEdit