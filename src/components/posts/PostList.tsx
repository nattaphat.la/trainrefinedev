import { Button, Col, EditButton, Form, Input, List, Row, ShowButton, Space, Table, useTable } from '@pankod/refine-antd'
import { CrudFilters } from '@pankod/refine-core'
import Link from 'next/link'
import React from 'react'
import { IPost } from 'src/interfaces/post'

type Props = {}

const PostList = (props: Props) => {
  const {
    tableProps,
    searchFormProps
  } = useTable({
    metaData: {
      fields: ['id', 'title']
    },
    onSearch: (data: any) => {
      const filters: CrudFilters = [];
      if (data.title) {
        filters.push({
          field: "title",
          operator: "contains",
          value: data.title,
        })
      }
      return filters
    }
  })
  return (
    <List>
      <Row gutter={[16,16]}>
        <Col xs={24}>
          <Form {...searchFormProps} layout="vertical">
            <Form.Item name="title" label="Title">
              <Input />
            </Form.Item>
            <Form.Item>
              <Button htmlType="submit" type="primary">Filter</Button>
            </Form.Item>
          </Form>
        </Col>
        <Col xs={24}>
          <Table {...tableProps}>
            <Table.Column dataIndex="id" title="ID" sorter />
            <Table.Column dataIndex="title" title="Title" sorter />
            <Table.Column title="Title" render={(record: IPost) => (<div>{record.id} {record.title}</div>)} />
            <Table.Column
              title={'Actions'}
              // dataIndex="actions"
              render={(record): React.ReactNode => {
                return (
                  <Space>
                    <EditButton
                      size="small"
                      recordItemId={record.id}
                    />
                    <ShowButton
                      size="small"
                      recordItemId={record.id}
                    />
                  </Space>
                );
              }}
            />
          </Table>
        </Col>
      </Row>
    </List>
  )
}

export default PostList