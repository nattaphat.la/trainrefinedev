import { Show, Tag, Typography } from '@pankod/refine-antd'
import { useShow } from '@pankod/refine-core'
import React from 'react'

type Props = {}

const PostShow = (props: Props) => {
  const {
    queryResult
  } = useShow({
    metaData: {
      fields: ['id', 'title', {
        tags: ['id', 'name']
      }]
    }
  })
  const { data, isLoading } = queryResult;
  const record = data?.data;
  return (
    <Show isLoading={isLoading}>
      <Typography.Title level={5}>ID</Typography.Title>
      <Typography.Text>{record?.id}</Typography.Text>

      <Typography.Title level={5}>Title</Typography.Title>
      <Typography.Text>{record?.title}</Typography.Text>

      <Typography.Title level={5}>Tags</Typography.Title>
      <Typography.Text>
        {record?.tags.map((tag:any) => <Tag>{tag.name}</Tag>)}
      </Typography.Text>
    </Show>
  )
}

export default PostShow