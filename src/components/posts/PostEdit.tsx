import { Edit, Form, Input, useForm } from '@pankod/refine-antd'
import React from 'react'

const PostEdit = (props: any) => {
  const {
    saveButtonProps,
    formProps
  } = useForm({
    metaData: {
      fields: ['id', 'title']
    }
  })
  return (
    <Edit {...props} saveButtonProps={saveButtonProps}>
      <Form {...formProps}>
        <Form.Item name="title" label="Title">
          <Input />
        </Form.Item>
      </Form>
    </Edit>
  )
}

export default PostEdit