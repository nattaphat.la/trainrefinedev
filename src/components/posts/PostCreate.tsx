import { Create, Form, Input, useForm } from '@pankod/refine-antd'
import React from 'react'

type Props = {}

const PostCreate = (props: Props) => {
  const {
    saveButtonProps,
    formProps
  } = useForm({
    redirect: 'list'
  })
  return (
    <Create saveButtonProps={saveButtonProps}>
      <Form {...formProps}>
        <Form.Item name="title" label="Title">
          <Input />
        </Form.Item>
      </Form>
    </Create>
  )
}

export default PostCreate