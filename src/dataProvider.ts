import { CrudFilters, CrudSorting, DataProvider } from "@pankod/refine-core";
import { GraphQLClient } from "graphql-request";
import * as gql from "gql-query-builder";
import pluralize from "pluralize";
import camelCase from "camelcase";

const genereteSort = (sort?: CrudSorting) => {
    if (sort && sort.length > 0) {
        const sortQuery = sort.reduce((prev, cur) => {
            // return `${i.field}:${i.order}`;
            return {
              ...prev,
              [cur.field]: cur.order
            }
        }, {});

        return sortQuery
    }

    return null;
};

const generateFilter = (filters?: CrudFilters) => {
    const queryFilters: { [key: string]: any } = {};

    if (filters) {
        filters.map((filter) => {
            if (filter.operator !== "or") {
                const { field, operator, value } = filter;

                queryFilters[`${field}`] = {
                  [operator]: value
                }
            } else {
                const { value } = filter;

                const orFilters: any = value.reduce((prev, cur) => {
                    // orFilters.push({
                    //     [`${val.field}_${val.operator}`]: val.value,
                    // });
                    return {
                      ...prev,
                      [`${cur.field}`]: {
                        [cur.operator]: value
                      }
                    }
                }, {});

                queryFilters["OR"] = orFilters;
            }
        });
    }

    return queryFilters;
};

const dataProvider = (client: GraphQLClient): DataProvider => {
    return {
        getList: async ({ resource, pagination, sort, filters, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelCaseOrderByType = camelCase(`${singularResource}OrderByInput`, {
              pascalCase: true
            });
            const camelCaseWhereType = camelCase(`${singularResource}WhereInput`, {
              pascalCase: true
            });
            const current = pagination?.current || 1;
            const pageSize = pagination?.pageSize || 10;

            const sortBy = genereteSort(sort);
            const filterBy = generateFilter(filters);

            const camelResource = camelCase(resource);

            const operation = metaData?.operation ?? camelResource;
            const where = { value: filterBy, type: `${camelCaseWhereType}!` }
            const skip = (current - 1) * pageSize
            const take = pageSize
            const countItemVariable: any = {
              where,
              ...metaData?.variables,
            }
            const queryItemVariable: any = {
              ...countItemVariable,
              skip,
              take
            }
            if (sortBy) {
              queryItemVariable['orderBy'] = { value: sortBy, type: `[${camelCaseOrderByType}!]!` }
            }
            const { query, variables } = gql.query([
              {
                operation: `item:${operation}`,
                variables: queryItemVariable,
                fields: metaData?.fields,
              },
              {
                operation: `count:${operation}Count`,
                variables: countItemVariable,
              }
            ]
            );

            const response = await client.request(query, variables);
            return {
                data: response.item,
                total: response.count,
            };
        },

        getMany: async ({ resource, ids, metaData }) => {
            const camelResource = camelCase(resource);

            const operation = metaData?.operation ?? camelResource;

            const { query, variables } = gql.query({
                operation,
                variables: {
                    where: {
                        value: { id_in: ids },
                        type: "JSON",
                    },
                },
                fields: metaData?.fields,
            });

            const response = await client.request(query, variables);

            return {
                data: response[operation],
            };
        },

        create: async ({ resource, variables, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelCreateName = camelCase(`create-${singularResource}`);

            const operation = metaData?.operation ?? camelCreateName;

            const { query, variables: gqlVariables } = gql.mutation({
                operation,
                variables: {
                    data: {
                        value: variables,
                        type: `${camelCase(`${singularResource}CreateInput`, {pascalCase: true})}!`,
                    },
                },
                fields: metaData?.fields ?? ['id'],
            });
            const response = await client.request(query, gqlVariables);
            return {
                data: response[operation],
            };
        },

        createMany: async ({ resource, variables, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelCreateName = camelCase(`create-${singularResource}`);

            const operation = metaData?.operation ?? camelCreateName;

            const response = await Promise.all(
                variables.map(async (param) => {
                    const { query, variables: gqlVariables } = gql.mutation({
                        operation,
                        variables: {
                            input: {
                                value: { data: param },
                                type: `${camelCreateName}Input`,
                            },
                        },
                        fields: metaData?.fields ?? [
                            {
                                operation: singularResource,
                                fields: ["id"],
                                variables: {},
                            },
                        ],
                    });
                    const result = await client.request(query, gqlVariables);

                    return result[operation][singularResource];
                }),
            );
            return {
                data: response,
            };
        },

        update: async ({ resource, id, variables, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelUpdateName = camelCase(`update-${singularResource}`);
            const whereType = camelCase(`${singularResource}WhereUniqueInput!`, {
                pascalCase: true
            })
            const updateType = camelCase(`${singularResource}UpdateInput!`, {
                pascalCase: true
            })
            const operation = metaData?.operation ?? camelUpdateName;

            const { query, variables: gqlVariables } = gql.mutation({
                operation,
                variables: {
                    where: {
                        value: { id },
                        type: whereType,
                    },
                    data: {
                        value: variables,
                        type: updateType
                    }
                },
                fields: metaData?.fields ?? ['id'],
            });
            const response = await client.request(query, gqlVariables);

            return {
                data: response[operation],
            };
        },

        updateMany: async ({ resource, ids, variables, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelUpdateName = camelCase(`update-${singularResource}`);

            const operation = metaData?.operation ?? camelUpdateName;

            const response = await Promise.all(
                ids.map(async (id) => {
                    const { query, variables: gqlVariables } = gql.mutation({
                        operation,
                        variables: {
                            input: {
                                value: { where: { id }, data: variables },
                                type: `${camelUpdateName}Input`,
                            },
                        },
                        fields: metaData?.fields ?? [
                            {
                                operation: singularResource,
                                fields: ["id"],
                                variables: {},
                            },
                        ],
                    });
                    const result = await client.request(query, gqlVariables);

                    return result[operation][singularResource];
                }),
            );
            return {
                data: response,
            };
        },

        getOne: async ({ resource, id, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelResource = camelCase(singularResource);
            const whereType = camelCase(`${singularResource}WhereUniqueInput`, {
              pascalCase: true
            })
            const operation = metaData?.operation ?? camelResource;
            const where = {
              value: {
                id: id
              },
              type: `${whereType}!`
            }
            const { query, variables } = gql.query({
                operation,
                variables: {
                  where
                },
                fields: metaData?.fields,
            });

            const response = await client.request(query, variables);

            return {
                data: response[operation],
            };
        },

        deleteOne: async ({ resource, id, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelDeleteName = camelCase(`delete-${singularResource}`);

            const operation = metaData?.operation ?? camelDeleteName;
            const whereType = camelCase(`${singularResource}WhereUniqueInput!`, {
                pascalCase: true
              })
            const { query, variables } = gql.mutation({
                operation,
                variables: {
                    where: {
                        value: { id },
                        type: whereType,
                    },
                },
                fields: metaData?.fields ?? ['id'],
            });

            const response = await client.request(query, variables);

            return {
                data: response[operation],
            };
        },

        deleteMany: async ({ resource, ids, metaData }) => {
            const singularResource = pluralize.singular(resource);
            const camelDeleteName = camelCase(`delete-${singularResource}`);

            const operation = metaData?.operation ?? camelDeleteName;

            const response = await Promise.all(
                ids.map(async (id) => {
                    const { query, variables: gqlVariables } = gql.mutation({
                        operation,
                        variables: {
                            input: {
                                value: { where: { id } },
                                type: `${camelDeleteName}Input`,
                            },
                        },
                        fields: metaData?.fields ?? [
                            {
                                operation: singularResource,
                                fields: ["id"],
                                variables: {},
                            },
                        ],
                    });
                    const result = await client.request(query, gqlVariables);

                    return result[operation][singularResource];
                }),
            );
            return {
                data: response,
            };
        },

        getApiUrl: () => {
            throw Error("Not implemented on refine-graphql data provider.");
        },

        custom: async ({ url, method, headers, metaData }) => {
            let gqlClient = client;

            if (url) {
                gqlClient = new GraphQLClient(url, { headers });
            }

            if (metaData) {
                if (metaData.operation) {
                    if (method === "get") {
                        const { query, variables } = gql.query({
                            operation: metaData.operation,
                            fields: metaData.fields,
                            variables: metaData.variables,
                        });

                        const response = await gqlClient.request(
                            query,
                            variables,
                        );

                        return {
                            data: response[metaData.operation],
                        };
                    } else {
                        const { query, variables } = gql.mutation({
                            operation: metaData.operation,
                            fields: metaData.fields,
                            variables: metaData.variables,
                        });

                        const response = await gqlClient.request(
                            query,
                            variables,
                        );

                        return {
                            data: response[metaData.operation],
                        };
                    }
                } else {
                    throw Error("GraphQL operation name required.");
                }
            } else {
                throw Error(
                    "GraphQL need to operation, fields and variables values in metaData object.",
                );
            }
        },
    };
};

export default dataProvider;